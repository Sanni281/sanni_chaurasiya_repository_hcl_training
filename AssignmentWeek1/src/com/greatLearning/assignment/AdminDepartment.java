package com.greatLearning.assignment;

public class AdminDepartment extends SuperDepartment {
	public String deparmentName() {
		return " Admin Department";
		}
		//declare method getTodaysWork of return type string
		public String getTodaysWork() {
		return " Complete your documents ";
		}
		//declare method getWorkDeadline of return type string
		public String getWorkDeadline() {
		return " Complete by EOD ";
		}
		@Override
		public String toString() {
			return "DeparmentName = " + deparmentName() + ", getTodaysWork = " + getTodaysWork()
					+ ", getWorkDeadline = " + getWorkDeadline() + ", isTodayAHoliday = " + isTodayAHoliday()+"\n";
		}
		
}
