create database travel_sanni_chaurasiya;

use travel_sanni_chaurasiya;

//passenger table//
->create table passenger
  (Passenger_name     varchar(20), 
   Category           varchar(20),
   Gender             varchar(20),
   Boarding_City      varchar(20),
   Destination_City   varchar(20),
   Distance           int,
   Bus_Type           varchar(20)
   );

insert into passenger values('Sejal','AC','F','Bengaluru','Chennai',350,'Sleeper');
 insert into passenger values('Anmol','Non-AC','M','Mumbai','Hyderabad',700,'Sitting');
 insert into passenger values('Pallavi','AC','F','panaji','Bengaluru',600,'Sleeper');
 insert into passenger values('Khusboo','AC','F','Chennai','Mumbai',1500,'Sleeper');
 insert into passenger values('Udit','Non-AC','M','Trivandrum','panaji',1000,'Sleeper');
 insert into passenger values('Ankur','AC','M','Nagpur','Hyderabad',500,'Sitting');
 insert into passenger values('Hemant','Non-AC','M','panaji','Mumbai',700,'Sleeper');
 insert into passenger values('Manish','Non-AC','M','Hyderabad','Bengaluru',500,'Sitting');
 insert into passenger values('Piyush','AC','M','Pune','Nagpur',700,'Sitting');

select *from passenger;

//price table//
->create table price
  (Bus_Type   varchar(20),
   Distance   int,
   Price      int
   );
->insert into price values('Sleeper',350,770);
 insert into price values('Sleeper',500,1100);
 insert into price values('Sleeper',600,1320);
 insert into price values('Sleeper',700,1540);
 insert into price values('Sleeper',1000,2200);
 insert into price values('Sleeper',1200,2640);
 insert into price values('Sleeper',350,434);
 insert into price values('Sitting',500,620);
 insert into price values('Sitting',500,620);
 insert into price values('Sitting',600,744);
 insert into price values('Sitting',700,868);
 insert into price values('Sitting',1000,1240);
 insert into price values('Sitting',1200,1488);
 insert into price values('Sitting',1500,1860);
->select *from price;

Solution:-


1. select gender,count(gender) from passenger where distance>=600 group by gender;

2.select min(price) from price where Bus_Type="Sleeper";

3.select Passenger_name from passenger WHERE Passenger_name LIKE 'S%';

4.select pgr.passenger_name, pgr.boarding_city, pgr.destination_city, pgr.bus_type, sum(pr.price) as ticket_price
    -> FROM
    ->   passenger pgr
    ->   inner join price pr on pgr.bus_type = pr.bus_type
    ->   AND pgr.distance = pr.distance
    ->   group by pgr.passenger_name;

5.  SELECT 
    pgr.passenger_name, pr.price AS 'ticket price'
    FROM passenger pgr
    JOIN price pr ON pgr.bus_type = 'sitting'
    AND pgr.distance = 1000
    GROUP BY pgr.passenger_name;

6.select pr.bus_type, pr.price from passenger pgr, price pr 
  where pgr.passenger_name = 'Pallavi' and pgr.distance = pr.distance;

7.select distinct(distance) from passenger order by distance desc;

8.Select passenger_name, (distance /(Select sum(distance) from passenger) * 100) as 'Percentage_of_Distance_travelled' from passenger;

9.create view view_passenger as
    -> SELECT Passenger_name,Category FROM passenger where Category='AC';
       SELECT Passenger_name,Category FROM view_passenger;

10.delimiter //
      create procedure get_Passenger()
    -> begin
    -> select passenger_name,Bus_Type from passenger where Bus_Type='Sleeper';
    -> end //
CALL get_Passenger()//
delimiter ;

11.Select *from passenger limit 5;
