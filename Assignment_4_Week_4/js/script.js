
    const loaddata = async() => {
        var response = await fetch("http://localhost:3000/movies-coming")
        var data = await response.json();
        var previous = document.querySelector('.row');
        previous.remove();
        const addrow = document.querySelector('.container-fluid');
        addrow.innerHTML = `<div class="row"  ></div>`;
        for (let i = 0; i < data.length; i++) {
            const movietable = document.querySelector('.row');
            movietable.innerHTML = movietable.innerHTML + `<div class="col-md-2 m-a-2">
			<div class="card">
				  <img class="card-img-top img-fixed" src="${data[i].posterurl}" onerror="this.src='https://motivatevalmorgan.com/wp-content/uploads/2016/06/default-movie.jpg';" alt="Card image cap">
				<div class="card-block">
					<h4 class="card-title">${data[i].title}</h4>
				</div>
				<ul class="list-group list-group-flush">
					<li class="list-group-item">Imdb Rating : ${data[i].imdbRating}</li>
				
					<li class="list-group-item">Release Date :${data[i].releaseDate}</li>
				</ul>
				<div class="card-block">
					<button type="button" class="btn btn-primary btn-responsive" onclick="add_to_favorite(${data[i].id},'${data[i].title}','${data[i].posterurl}','${data[i].year}','${data[i].releaseDate}');">Add to favorite</button>
				</div>
			</div>
			</div>
			
			`

			
			
			
			
			
			
			
			
			
			
			
			
			
	
        }
    }
    const loaddata_movies_in_Theaters = async() => {
        const response = await fetch("http://localhost:3000/movies-in-theaters")
        const data = await response.json();
        var previous = document.querySelector('.row');
        previous.remove();
        const addrow = document.querySelector('.container-fluid');
        addrow.innerHTML = `<div class="row"></div`;
        for (let i = 0; i < data.length; i++) {
            const movietable = document.querySelector('.row');
            movietable.innerHTML = movietable.innerHTML + `<div class="col-md-2 m-a-2">
        <div class="card">
              <img class="card-img-top img-fixed" src="${data[i].posterurl}" onerror="this.src='https://motivatevalmorgan.com/wp-content/uploads/2016/06/default-movie.jpg';" alt="Card image cap">
            <div class="card-block">
                <h4 class="card-title">${data[i].title}</h4>
            </div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item">Imdb Rating : ${data[i].imdbRating}</li>
            
                <li class="list-group-item">Release Date :${data[i].releaseDate}</li>
            </ul>
            <div class="card-block">
                <button type="button" class="btn btn-primary btn-responsive" onclick="add_to_favorite(${data[i].id},'${data[i].title}','${data[i].posterurl}','${data[i].year}','${data[i].releaseDate}');">Add to favorite</button>
            </div>
        </div>
        </div>
        `
        }
    }
    const loaddata_top_rated_india = async() => {
        const response = await fetch("http://localhost:3000/top-rated-india")
        const data = await response.json();
        var previous = document.querySelector('.row');
        previous.remove();
        const addrow = document.querySelector('.container-fluid');
        addrow.innerHTML = `<div class="row"></div`;
        for (let i = 0; i < 30; i++) {
            const movietable = document.querySelector('.row');
            movietable.innerHTML = movietable.innerHTML + `<div class="col-md-2 m-a-2">
        <div class="card">
            <img class="card-img-top img-fixed" src="${data[i].posterurl}" onerror="this.src='https://motivatevalmorgan.com/wp-content/uploads/2016/06/default-movie.jpg';" alt="Card image cap">
            <div class="card-block">
                <h4 class="card-title">${data[i].title}</h4>
            </div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item">Imdb Rating : ${data[i].imdbRating}</li>
                <li class="list-group-item">Release Date :${data[i].releaseDate}</li>
            </ul>
            <div class="card-block">
                <button type="button" class="btn btn-primary btn-responsive" onclick="add_to_favorite(${data[i].id},'${data[i].title}','${data[i].posterurl}','${data[i].year}','${data[i].releaseDate}');">Add to favorite</button>
            </div>
        </div>
        </div>
`
        }
    }
    const loaddata_top_rated_movies = async() => {
        const response = await fetch("http://localhost:3000/top-rated-movies")
        const data = await response.json();
        var previous = document.querySelector('.row');
        previous.remove();
        const addrow = document.querySelector('.container-fluid');
        addrow.innerHTML = `<div class="row"></div`;
        for (let i = 0; i < 30; i++) {
            const movietable = document.querySelector('.row');
            movietable.innerHTML = movietable.innerHTML + `<div class="col-md-2 m-a-2">
        <div class="card">
            <img class="card-img-top img-fixed" src="${data[i].posterurl}" onerror="this.src='https://motivatevalmorgan.com/wp-content/uploads/2016/06/default-movie.jpg';" alt="Card image cap">
            <div class="card-block">
                <h4 class="card-title">${data[i].title}</h4>

            </div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item">Imdb Rating : ${data[i].imdbRating}</li>
                <li class="list-group-item">Release Date :${data[i].releaseDate}</li>
            </ul>
            <div class="card-block">
                <button type="button" class="btn btn-primary btn-responsive" onclick="add_to_favorite(${data[i].id},'${data[i].title}','${data[i].posterurl}','${data[i].year}','${data[i].releaseDate}');">Add to favorite</button>
            </div>
        </div>
        </div>
        `
        }
    }

    const loaddata_favorite = async() => {
        const response = await fetch("http://localhost:3000/favourit")
        const data = await response.json();
        var previous = document.querySelector('.row');
        previous.remove();
        const addrow = document.querySelector('.container-fluid');
        addrow.innerHTML = `<div class="row"></div`;
        for (let i = 0; i < 30; i++) {

            const movietable = document.querySelector('.row');
            movietable.innerHTML = movietable.innerHTML + `<div class="col-md-2 m-a-2">

        <div class="card">
            <img class="card-img-top img-fixed" src="${data[i].posterurl}" onerror="this.src='https://motivatevalmorgan.com/wp-content/uploads/2016/06/default-movie.jpg';"  alt="Card image cap">
            <div class="card-block">
                <h4 class="card-title">${data[i].title}</h4>
            </div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item">Id : ${data[i].id}</li>
                <li class="list-group-item">Release Date :${data[i].releasedate}</li>
            </ul>
            <div class="card-block">
                <button type="button" onclick="removeItem(${data[i].id});" class="btn btn-primary btn-responsive">Remove</button>
            </div>
        </div>
        </div>

`
        }
    }

    function add_to_favorite(a, b, c, d, e) {
        alert(b + " Is added to Favorite");
        fetch('http://localhost:3000/favourit', {
                method: 'post',
                headers: {
                    'Accept': 'application/json, text/plain, /',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    currentid: a,
                    title: b,
                    posterurl: c,
                    year: d,
                    releasedate: e
                })
            }).then(res => res.json())
            .then(res => console.log(res));
    }